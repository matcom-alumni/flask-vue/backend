"""Module containing all factory methods"""

from flask import Flask
from .database import db_session
from .extensions import admin, cors
from .views import app_views


def create_app():
    """API app factory"""
    _app = Flask(__name__)
    admin.init_app(_app)
    cors.init_app(_app)
    _app.register_blueprint(app_views)
    return _app


app = create_app()


@app.teardown_appcontext
def shutdown_session(exception=None):
    """
    Method for remove database sessions at the end of
    the request or when the application shuts down.
    """
    if exception:
        print(str(exception))
    db_session.remove()
