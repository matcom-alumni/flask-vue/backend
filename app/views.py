"""Module containing all views"""

from flask import Blueprint, jsonify
from flask_cors import cross_origin
from .database import User


app_views = Blueprint('app_views', __name__)

@app_views.route('/', methods=['GET'])
def home():
    """Hello World page"""
    return 'HELLO WORLD'


@app_views.route('/vue', methods=['GET'])
@cross_origin()
def vue_demo():
    """Vue demo page"""
    return 'VUE NEEDS CORS FLASK-CORS'


@app_views.route('/users', methods=['GET'])
@cross_origin()
def get_users():
    """Example of endpoint for obtain data from database"""
    users = User.query.all()
    return jsonify(users)
