"""Module containing all global objects"""

from flask_cors import CORS
from .admin import admin

cors = CORS()
