"""Module containing admin setup"""

from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from .database import db_session, User

admin = Admin(name='DisperZion', template_mode='bootstrap3')
admin.add_view(ModelView(User, db_session))
