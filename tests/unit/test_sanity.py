"""This module contains all sanity tests"""

import unittest


class TestSanity(unittest.TestCase):
    """This class contains all sanity tests"""
    def test_math_sanity(self):
        """This is a simple math sanity test to check whether there is a problem running
            tests or not
        """
        self.assertEqual(12 * 11, 132, "There is a big error with tests")
